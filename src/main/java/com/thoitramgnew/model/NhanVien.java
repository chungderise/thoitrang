package com.thoitramgnew.model;

public class NhanVien {
    int idNhanVien;
    String name;
    int age;
    public int getIdNhanVien() {
        return idNhanVien;
    }
    public void setIdNhanVien(int idNhanVien) {
        this.idNhanVien = idNhanVien;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    public int getAge() {
        return age;
    }
    public void setAge(int age) {
        this.age = age;
    }
    public NhanVien() {
        super();
    }
    public NhanVien(int idNhanVien, String name, int age) {
        super();
        this.idNhanVien = idNhanVien;
        this.name = name;
        this.age = age;
    }
    @Override
    public String toString() {
        return "NhanVien [idNhanVien=" + idNhanVien + ", name=" + name + ", age=" + age + "]";
    }

    
}
