package com.thoitramgnew.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.thoitramgnew.model.NhanVien;

@Repository
public class DatabaseThoiTrang {
    private JdbcTemplate jsJdbcTemplate;

    @Autowired
    public void setDataSource(DataSource dataSource) {
        this.jsJdbcTemplate = new JdbcTemplate(dataSource);
    }

    public List<NhanVien> getListNhanVien() {
        String sql = "SELECT * FROM NHANVIEN";
        List<NhanVien> listNhanVien = jsJdbcTemplate.query(sql, new RowMapper<NhanVien>() {
            public NhanVien mapRow(ResultSet rs, int rowNum) throws SQLException {
                NhanVien nv = new NhanVien();
                nv.setName(rs.getString("name"));
                nv.setAge(Integer.valueOf(rs.getString("age")));
                return nv;
            }
        });

        for (NhanVien valueNV : listNhanVien) {
            System.out.println("Gia tri : " + valueNV.getName() + " _ " +  valueNV.getAge());
        }
        return listNhanVien;
    }

}
