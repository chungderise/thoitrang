package com.example.dao;

import com.example.model.Sale;

public interface SaleMapper {
    int deleteByPrimaryKey(Short id);

    int insert(Sale record);

    int insertSelective(Sale record);

    Sale selectByPrimaryKey(Short id);

    int updateByPrimaryKeySelective(Sale record);

    int updateByPrimaryKey(Sale record);
}