package com.thoitrangnew.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.thoitramgnew.dao.DatabaseThoiTrang;
import com.thoitramgnew.model.NhanVien;

@Controller
@RequestMapping("/")
public class TrangChuController {
    private ApplicationContext context;

    @GetMapping
    public String Default(ModelMap model) {
        List<NhanVien> listNV = new ArrayList<NhanVien>();
        context = new ClassPathXmlApplicationContext("IoC.xml");

        DatabaseThoiTrang databaseThoiTrang = (DatabaseThoiTrang) context.getBean("datathoitrang");
        listNV =  databaseThoiTrang.getListNhanVien();
        model.addAttribute("nv", listNV);

        return "trangchu";
    }
}
